#pragma GCC optimize "Ofast"

#ifdef LOCAL

// #include "extra.h"

#endif

#include <iostream>
#include <string>
#include <vector>
#include <algorithm>
#include <stdint.h>
#include <unordered_set>
#include <iostream>
#include <string>
#include <assert.h>
#include <array>
#include <queue>
#include <unordered_map>
#include <math.h>
#include <time.h>

#ifdef _MSVC_LANG
#include <intrin.h>
#endif

using namespace std;

array<double, 10000> log_;

inline int hrand() // random int [0 ... 32767]
{
    static unsigned int m_w = 1;
    m_w = 18000 * (m_w & 65535) + (m_w >> 16);
    return m_w & 32767;
}

unordered_set<int> generated;
vector<int> winner;
vector<int> perspectiveWinner;
array<vector<vector<int>>, 3> allPossibleStates;

struct BigState: public array<int, 9> {
    BigState()
        : array<int, 9>{0}
          , zone_(-1) {
        updateHashes();
    }

    BigState(int a, int z)
        : array<int, 9>{a}
          , zone_(z) {
        updateHashes();
    }

    void updateHashes() {
        hash_ = zone_;
        for (auto& h : hashes) {
            h = 0;
        }
        hashes[0] = zone_ + 1;
        for (int i = 0; i < 9; i++) {
            hash_ ^= (*this)[i];
            hash_ *= 2654435761;

            hashes[i % 3] <<= 18;
            hashes[i % 3] |= (*this)[i];
        }
    }

    uint64_t hash() const {
        // assert(hash_ != -1);
        return hash_;
    }

    bool operator<(const BigState& op) const {
        if (zone_ < op.zone_) {
            return true;
        }
        if (zone_ > op.zone_) {
            return false;
        }

        for (int i = 0; i < 9; i++) {
            if ((*this)[i] < op[i]) {
                return true;
            }
            if ((*this)[i] > op[i]) {
                return false;
            }
        }
        return false;
    }

    bool eq1(const BigState& op1) {
        for (int i = 0; i < 3; i++) {
            if (hashes[i] != op1.hashes[i]) {
                return false;
            }
        }
        return true;
    }

    bool eq2(const BigState& op1) {
        if (op1.zone_ != zone_) {
            return false;
        }
        for (size_t i = 0; i < size(); i++) {
            if (op1[i] != (*this)[i]) {
                return false;
            }
        }
        return true;
    }

    bool operator==(const BigState& op1) {
        for (int i = 0; i < 3; i++) {
            if (hashes[i] != op1.hashes[i]) {
                return false;
            }
        }
        return true;
        /*
        if (hash2_ != op1.hash2_) {
        return false;
        }
        if (hash3_ != op1.hash3_) {
        return false;
        }
        if (hash_ != op1.hash_) {
        return false;
        }
        return true;
        */
        /*
        if (op1.zone_ != zone_) {
        return false;
        }
        for (size_t i = 0; i < size(); i++) {
        if (op1[i] != (*this)[i]) {
        return false;
        }
        }
        return true;
        */
    }

    void setZone(int z) {
        zone_ = z;
        updateHashes();
    }

    int zone() const {
        return zone_;
    }

private:
    int zone_ = -1;
    uint64_t hash_ = -1;
    array<uint64_t, 3> hashes;
};

template<class ValueType>
class FastHashMap {
public:
    FastHashMap(size_t size_ = 1 << 21)
        : size(size_) {
        arr_ = new vector<pair<BigState, ValueType>>[size];
    }

    ~FastHashMap() {
        if(arr_ != nullptr) {
            delete[] arr_;
            arr_ = nullptr;
        }
    }

    void emplace(const BigState& s, ValueType&& val) {
        auto& v = arr_[s.hash() % size];

        total++;
        if (!v.empty()) {
            collisions++;
        }
        v.emplace_back(s, val);
    }

    ValueType* end() const {
        return nullptr;
    }

    ValueType* find(const BigState& s) {
        auto& v = arr_[s.hash() % size];
        for (auto& bs : v) {
            if (bs.first == s) {
                return &(bs.second);
            }
        }

        return nullptr;
    }

    ValueType& operator[](const BigState& s) {
        auto& v = arr_[s.hash() % size];
        //		if (v.size() == 1) {
        //			return v.front().second;
        //		}
        if (!v.empty()) {
            for (auto& bs : v) {
                if (bs.first == s) {
                    return bs.second;
                }
            }
        }

        total++;
        if (!v.empty()) {
            collisions++;
        }
        v.emplace_back(s, ValueType());
        return v.back().second;
    }

    uint32_t total = 0;
    uint32_t collisions = 0;
private:
    size_t size = -1;
    ValueType fake;
    vector<pair<BigState, ValueType>>* arr_ = nullptr;
};

class Bot {
public:
    virtual BigState doTurn(const BigState& state, int player) = 0;
};

string visualize(int state) {
    string ret;
    static char mark[4] = "_XO";
    for (int i = 16; i >= 0; i -= 2) {
        int pos = (state >> i) & 3;
        ret += mark[pos];
        if (i % 3 == 0) {
            ret += '\n';
        }
    }

    return ret;
}

string visualize(BigState state) {
    string ret;
    ret = "zone: " + to_string(state.zone()) + "\n";
    for (auto& s : state) {
        ret += visualize(s);
        ret += "\n";
    }
    return ret;
}

int checkWin(int state) {
    for (int player : {1, 2}) {
        int hor = player;
        hor <<= 2;
        hor |= player;
        hor <<= 2;
        hor |= player;

        for (int i = 0; i < 3; i++) {
            if (hor == (hor & state)) {
                return player;
            }
            hor <<= 6;
        }

        int ver = player;
        ver <<= 6;
        ver |= player;
        ver <<= 6;
        ver |= player;
        for (int i = 0; i < 3; i++) {
            if (ver == (ver & state)) {
                return player;
            }
            ver <<= 2;
        }

        int diag1 = player | (player << 8) | (player << 16);
        if (diag1 == (diag1 & state)) {
            return player;
        }
        int diag2 = (player << 4) | (player << 8) | (player << 12);
        if (diag2 == (diag2 & state)) {
            return player;
        }
    }
    for (int i = 0; i < 9; i++) {
        if ((state & 3) == 0) {
            // We still can make a move here;
            return 0;
        }
        state >>= 2;
    }
    // It's a draw!
    return -1;
}

vector<int> genPossibleStates(int state, int player) {
    vector<int> ret;

    if (checkWin(state) > 0) {
        return {};
    }

    ret.reserve(9);
    int offset = 3;
    for (int i = 0; i < 18; i += 2) {
        if (!(state & offset)) {
            //            int newState = state | (player << i);
            ret.push_back(state | (player << i));
        }
        offset <<= 2;
    }
    return ret;
}

void genAllStates() {
    for (auto& ps : allPossibleStates) {
        ps.clear();
        ps.resize(1 << 18);
    }
    winner.resize(1 << 18, 0);

    vector<int> cur_states;
    vector<int> new_states;
    cur_states.push_back(0);
    generated.insert(0);

    while (!cur_states.empty()) {
        new_states.clear();
        new_states.reserve(cur_states.size());

        while (!cur_states.empty()) {
            auto& cur = cur_states.back();
            winner[cur] = checkWin(cur);
            if (winner[cur] == 0) {
                for (int player : {1, 2}) {
                    allPossibleStates[player][cur] =
                        move(genPossibleStates(cur, player));

                    for (const auto& g : allPossibleStates[player][cur]) {
                        if (generated.find(g) == generated.end()) {
                            generated.insert(g);
                            new_states.push_back(g);
                        }
                    }
                }
            }
            cur_states.pop_back();
        }
        cur_states = move(new_states);
    }

    cerr << "Total states: " << generated.size() << endl;
}

/*
template<BigState>
struct hash<BigState> {
std::size_t operator()(const BigState& k) const
{
size_t h = k.zone;
for (const auto& p : k) {
h ^= p;
h *= 2654435761;
}

return h;
}
};
*/

FastHashMap<int> bigWinner;

int checkBigWin(const BigState& bigState) {
    auto hope = bigWinner.find(bigState);
    if (hope != bigWinner.end()) {
        return *hope;
    }
    auto& ret = bigWinner[bigState];

    int abstractState = 0;
    int availableTurns = 0;
    for (const auto& s : bigState) {
        abstractState <<= 2;
        auto& win = winner[s];
        if (win > 0) {
            abstractState |= win;
        }
        if (win == 0) {
            availableTurns++;
        }
    }
    if (winner[abstractState] > 0) {
        ret = winner[abstractState];
        return ret;
    }
    if (availableTurns > 0) {
        // We still can continue the game
        ret = 0;
        return ret;
    }
    array<int, 3> pts{0};
    while (abstractState > 0) {
        pts[abstractState & 3]++;
        abstractState >>= 2;
    }
    if (pts[1] > pts[2]) {
        ret = 1;
        return ret;
    }
    if (pts[1] < pts[2]) {
        ret = 2;
        return ret;
    }

    // It's a draw!
    ret = -1;
    return ret;
}

int zoneByDiff(int prev, int post) {
    int diff = prev ^post;
#ifdef _MSVC_LANG
    return 15 - __lzcnt(diff) / 2;
#else
    return __builtin_ctz(diff) / 2;
#endif // MSVC
}

FastHashMap<vector<BigState>>* allPossibleBigStates;

vector<BigState> getPossibleBigStates(const BigState& state, int player, vector<BigState>* ret_vector = nullptr) {

    if (checkBigWin(state) > 0) {
        return vector<BigState>();
    }

    vector<BigState> ret;
    if ((state.zone() == -1) ||
        (allPossibleStates[player][state[state.zone()]].empty())) {
        ret.reserve(30);
        for (size_t i = 0; i < state.size(); i++) {
            if (!allPossibleStates[player][state[i]].empty()) {
                auto tmp_state = state;
                tmp_state.setZone(i);
                getPossibleBigStates(tmp_state, player, &ret);
            }
        }
    } else {
        int prevState = state[state.zone()];
        auto& possibleStates = allPossibleStates[player][prevState];
        if (ret_vector == nullptr) {
            ret.reserve(possibleStates.size());
        }
        for (auto& ps : possibleStates) {
            auto tmp_state = state;
            tmp_state[state.zone()] = ps;
            if (winner[ps] != 0) {
                tmp_state.setZone(-1);
            } else {
                tmp_state.setZone(zoneByDiff(prevState, ps));
            }
            if (ret_vector == nullptr) {
                ret.emplace_back(tmp_state);
            } else {
                ret_vector->emplace_back(tmp_state);
            }
        }
    }
    return ret;
}
/*
vector<BigState>& getPossibleBigStates(const BigState& state, int player) {
    auto hope = allPossibleBigStates[player].find(state);
    if (hope != allPossibleBigStates[player].end()) {
        return *hope;
    }

    if (checkBigWin(state) > 0) {
        allPossibleBigStates[player].emplace(state, vector<BigState>());
        return allPossibleBigStates[player][state];
    }

    vector<BigState> ret;
    if ((state.zone() == -1) ||
        (allPossibleStates[player][state[state.zone()]].empty())) {
        ret.reserve(30);
        for (size_t i = 0; i < state.size(); i++) {
            if (!allPossibleStates[player][state[i]].empty()) {
                auto tmp_state = state;
                tmp_state.setZone(i);
                auto partial_ret = getPossibleBigStates(tmp_state, player);
                for (auto& p : partial_ret) {
                    ret.emplace_back(p);
                }
            }
        }
    } else {
        int prevState = state[state.zone()];
        auto& possibleStates = allPossibleStates[player][prevState];
        ret.reserve(possibleStates.size());
        for (auto& ps : possibleStates) {
            auto tmp_state = state;
            tmp_state[state.zone()] = ps;
            if (winner[ps] != 0) {
                tmp_state.setZone(-1);
            } else {
                tmp_state.setZone(zoneByDiff(prevState, ps));
            }
            ret.emplace_back(tmp_state);
        }
    }

    allPossibleBigStates[player].emplace(state, move(ret));
    return allPossibleBigStates[player][state];
}
 */

//void genAllBigStates() {
//    queue<pair<BigState, int> > cur_states;
//
//    cur_states.emplace(BigState(0, 0), 1 );
//    int player = 1;
//    while (!cur_states.empty()) {
//        auto& [cur, player] = cur_states.front();
//        int curWin = checkWin(cur);
//
//        if (curWin == 0) {
//
//            const auto& states = getPossibleBigStates(cur, player);
//
//            for (const auto&s : states) {
//                if (allPossibleBigStates[player].find(s) == allPossibleBigStates[player].end()) {
//                    cur_states.emplace(s, 3 - player);
//                }
//            }
//        }
//        cur_states.pop();
//    }
//
//    cerr << "Total states: " << allPossibleBigStates.size() << endl;
//}

pair<int, int> stateDiffToCoords(int diff) {
    int pos = 8;
    while ((diff & 3) == 0) {
        pos--;
        diff >>= 2;
        assert(pos >= 0);
    }
    return {pos / 3, pos % 3};
}

/*
class rngBot : public Bot {
public:
void doTurn(BigState& state, int player) override {
auto& possibleStates = getPossibleBigStates(state, player);
assert(!possibleStates.empty());

state = possibleStates[hrand() % possibleStates.size()];
}
};
*/

class mctsBot: public Bot {
public:
    mctsBot() {
        wins_ = new FastHashMap<uint32_t>[2];
        wins = wins_ - 1;
    }

    ~mctsBot() {
        if(wins_ !=  nullptr) {
            delete[] wins_;
            wins_ = nullptr;
        }
    }

    int dfs(const BigState& state, int player) {
        int win = checkBigWin(state);
        if(win != 0) {
            if (win > 0) {
                wins[win][state]++;
            }
            return win;
        }

        const auto& possibleStates = getPossibleBigStates(state, player);
        auto& totalParent = totalGames[state];
        totalParent++;

        double best_pts = 0;
        vector<size_t> pretenders;
        pretenders.reserve(possibleStates.size());
        for (size_t i = 0; i < possibleStates.size(); i++) {
            auto& posState = possibleStates[i];
            auto totalPretender = (double)totalGames[posState];
            double pts = 0.0;
            if (totalPretender > 0) {
                double wr = ((double)wins[player][posState]) / (totalPretender);
                assert(N < log_.size());
                pts = wr + C * sqrt(log_[totalParent] / totalPretender);
            }
            if (pts > best_pts) {
                best_pts = pts;
                pretenders.resize(0);
            }
            if (pts == best_pts) {
                pretenders.push_back(i);
            }
        }
        win = dfs(
            possibleStates[pretenders[hrand() % pretenders.size()]],
            3 - player);
        if (win > 0) {
            wins[win][state]++;
        }
        return win;

    }

    BigState doTurn(const BigState& state, int player) override {
#ifdef LOCAL
        for (int i = 0; i < 2000; i++) {
            dfs(state, player);
        }
#else
        auto ts = clock();
        while(((double)(clock() - ts)) * 1e3 / CLOCKS_PER_SEC < 95){
            dfs(state, player);
        }
#endif
        const auto& possibleStates = getPossibleBigStates(state, player);
        double best = 0;
        size_t best_i = 0;
        for (size_t i = 0; i < possibleStates.size(); i++) {
            double games = totalGames[possibleStates[i]];
            double wr = (double)wins[player][possibleStates[i]] / games;
            double pts = wr;
            if (pts > best) {
                best_i = i;
                best = pts;
            }
        }

        return possibleStates[best_i];
    }

    FastHashMap<uint32_t>* wins = nullptr;
    FastHashMap<uint32_t>* wins_  = nullptr;
    FastHashMap<uint32_t> totalGames;
    double C = 1.21;
    int dfsSteps = 1000;
};

int duel(Bot* p1, Bot* p2) {
    BigState state;
    vector<Bot*> bots = {(Bot*)nullptr, p1, p2};
    int player = 1;
    int win = 0;
    while (win == 0) {
        state = bots[player]->doTurn(state, player);
        win = checkBigWin(state);
        player = 3 - player;
    }
    return win;
}

pair<int, int> duels(Bot* p1, Bot* p2, int games = 2) {
    vector<Bot*> bots = {(Bot*)nullptr, p1, p2};
    pair<int, int> ret{0, 0};

    for (int game = 0; game < games / 2; game++) {
        BigState state;
        int player = 1;
        int win = 0;
        while (win == 0) {
            state = bots[player]->doTurn(state, player);
            win = checkBigWin(state);
            player = 3 - player;
        }
        if (win == 1) {
            ret.first++;
        }
        if (win == 2) {
            ret.second++;
        }
    }
    bots = {(Bot*)nullptr, p2, p1};
    for (int game = 0; game < games / 2; game++) {
        BigState state;
        int player = 1;
        int win = 0;
        while (win == 0) {
            state = bots[player]->doTurn(state, player);
            win = checkBigWin(state);
            player = 3 - player;
        }
        if (win == 2) {
            ret.first++;
        }
        if (win == 1) {
            ret.second++;
        }
    }
    return ret;
}

pair<int, int> bigStateDiff(const BigState& prev, const BigState& post) {
    for (int i = 0; i < 9; i++) {
        int diff = prev[i] ^post[i];
        if (diff > 0) {
            auto ret = stateDiffToCoords(diff);
            ret.first += 3 * (i / 3);
            ret.second += 3 * (i % 3);
            return ret;
        }
    }

    return {-1, -1};
}

int main() {
    for(size_t i = 0; i < log_.size(); i++) {
        log_[i] = log((double) i);
    }
    auto allPossibleBigStates_ = new FastHashMap<vector<BigState>>[2];
    allPossibleBigStates = allPossibleBigStates_ - 1;
    auto p1 = allPossibleBigStates + 1;
    auto p2 = allPossibleBigStates + 2;

    genAllStates();
    /*
    for (const auto& p : { 1, 2 }) {
    allPossibleBigStates[p].reserve(10000000);
    }
    */
    mctsBot bot;

#ifdef LOCAL
    mctsBot bot2;

    auto ts = clock();
    auto res = duels(&bot, &bot2);
    cout << res.first << " " << res.second << endl;

    cerr << "Time spent: " << ((double)(clock() - ts)) * 1e3 / CLOCKS_PER_SEC
         << " ms" << endl;

    delete[] allPossibleBigStates_;
    return 0;
#endif

    BigState bigState;

    int me = 2;
    int opp = 1;
    // game loop
    while (1) {
        int opponentRow;
        int opponentCol;
        cin >> opponentRow >> opponentCol;
        cin.ignore();

        int bigx = 1;
        int bigy = 1;
        int valx = 1;
        int valy = 1;

        if (opponentRow == -1) {
            me = 1;
            opp = 2;
            bigState.setZone(-1);
        } else {
            bigx = opponentCol / 3;
            bigy = opponentRow / 3;
            auto& curb = bigState[bigx + bigy * 3];

            valx = opponentCol % 3;
            valy = opponentRow % 3;
            curb |= opp << (6 * (2 - valy) + 2 * (2 - valx));
            if (winner[bigState[valx + valy * 3]] != 0) {
                bigState.setZone(-1);
            } else {
                bigState.setZone(valx + valy * 3);
            }
        }


        cerr << "Cur: \n" << visualize(bigState);

        int validActionCount;
        cin >> validActionCount;
        cin.ignore();
        for (int i = 0; i < validActionCount; i++) {
            int row;
            int col;
            cin >> row >> col;
            cin.ignore();
        }
        auto ts = clock();

        auto newState = bot.doTurn(bigState, me);

        cerr << "Time spent: " << ((double)(clock() - ts)) * 1e3 / CLOCKS_PER_SEC
             << " ms" << endl;
        cerr << "newState:\n" << visualize(newState);
        // cerr << "Zone: " << bigState.zone << endl;
        auto changes = bigStateDiff(bigState, newState);

        cout << changes.first << " " << changes.second << endl;
        bigState = newState;
    }

    allPossibleBigStates++;
    delete allPossibleBigStates;
    return 0;
}